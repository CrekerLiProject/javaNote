package tool;

/**print tool
 *
 * 几个概念：
 * 1.重载：可以理解为同一个方法名，不同的参数
 * 2.单例模式
 * Created by liqiang on 2018/8/25.
 */
public class Log {
    public Log(){

    }
    /**
     * null
     */
    public void p(){

    }
    /**
     * new line
     */
    private void pln(){
        System.out.println();
    }
    /**
     * obj
     */
    public void p(final Object src){
        System.out.print(String.valueOf(src));
    }
    /**
     * obj []
     */
    public void p(final Object [] src){
        for (Object aSrc : src) {
            p(aSrc);
        }
    }
    public void pln(final Object [] src){
        p(src);
        pln();
    }

    /**
     * boolean
     */
    public void p(final boolean src){
        System.out.print(src ? "true" : "false");
    }
    public void pln(final boolean src){
        p(src);
        pln();
    }

    /**
     * string
     */
    public void p(final String src){
        System.out.print(src);
    }
    public void pln(final String src){
        p(src);
        pln();
    }
    /**
     * char
     */
    public void p(final char c){
        System.out.print(String.valueOf(c));

    }
    public void pln(final char c){
        p(c);
        pln();
    }
    /**
     * int
     */
    public void p(final int i){
        System.out.print(String.valueOf(i));
    }
    public void pln(final int i){
        p(i);
        pln();
    }
    /**
     * long
     */
    public void p(final long l) {
        System.out.print(String.valueOf(l));
    }
    public void pln(final long l) {
        p(l);
        pln();
    }
    /**
     * float
     */
    public void p(final float f) {
        System.out.print(String.valueOf(f));
    }
    public void pln(final float f){
        p(f);
        pln();
    }
    /**
     * double
     */
    public void p(final double d) {
        System.out.print(String.valueOf(d));
    }
    public void pln(final double d) {
        p(d);
        pln();
    }

        /**
         * char []
         */
    public void p(final String head,final char [] c){
        p(head);
        p(String.valueOf(c));
    }
    public void pln(final String head,final char [] c){
        p(head,c);
        pln();
    }

    /**
     * int []
     */
    public void p(final String head,final int [] src){
        p(head);
        for(int i=0;i<src.length;i++){
            p(src[i]);
        }
    }
    public void pln(final String head,final int [] src){
        p(head,src);
        pln();
    }
    /**
     * long []
     */
    public void p(final String head,final long [] src){
        p(head);
        for (long aSrc : src) {
            p(aSrc);
        }
    }
    public void pln(final String head,final long [] src){
        p(head,src);
        pln();
    }
    /**
     * float []
     */
    public void p(final String head,final float [] src){
        p(head);
        for (float aSrc : src) {
            p(aSrc);
        }
    }
    public void pln(final String head,final float [] src){
        p(head, src);
        pln();
    }
    /**
     * double []
     */
    public void p(final String head,final double [] src){
        p(head);
        for (double aSrc : src) {
            p(aSrc);
        }
    }
    public void pln(final String head,final double [] src){
        p(head, src);
        pln();
    }
}
