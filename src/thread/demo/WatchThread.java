package thread.demo;

public class WatchThread extends Thread{
	private static final int OUT = 2;
	 public void run() {
		 int count = 0;
		 while(true) {
			 //已经开始测试、定时检查
			 Print.p("WatchThread 启动一轮监控");
			 while(TestThread.getIsStart()) {
				 try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				 count++;
				 Print.p("WatchThread 监控进行中 "+count);
				 //超时同时未结束
				 if(count > OUT && (TestThread.getIsEnd() == false)) {
					 Print.p("WatchThread 监控到超时  ");
					 TestThread.stopThread(DemoT.getmTestthread());
					 Print.p("WatchThread 监控到超时 停止当前线程 ");
					 count = 0;
					 break;
				 }
			 }
			 count = 0;
			 if(TestThread.getmIsAllTestEnd()) {
				 Print.p("WatchThread 停止所有监控");
				 break;
			 }
		 }
	}


}
