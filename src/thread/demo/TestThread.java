package thread.demo;

public class TestThread extends Thread{
	private static boolean mIsAllTestEnd = false;
	
	private static long mStart = 1;
	private static boolean mIsStart = false;
	
	private static long mEnd = 1;
	private static boolean mIsEnd = true;
	
	public synchronized static boolean getmIsAllTestEnd() {
		return mIsAllTestEnd;
	}
	
	public synchronized static long getStart() {
		return mStart;
	}
	public synchronized static boolean getIsStart() {
		return mIsStart;
	}
	public synchronized static long getEnd() {
		return mEnd;
	}
	public synchronized static boolean getIsEnd() {
		return mIsEnd;
	}
	
    public void run() {
    	for(int i = 0;i<2;i++) {
    		
    		mStart = System.currentTimeMillis();
    		mIsStart = true;
    		mIsEnd = false;
    		
        	boolean b = testThread();
        	
    		mEnd = System.currentTimeMillis();
    		mIsStart = false;
    		mIsEnd = true;
        	if(!b) {
            	Print.p("testThread 执行 run 异常 " + i);
        	}
    	}
    	mIsAllTestEnd = true;
    	
    }
    
    /**
     * 执行的测试用例方法
     * @return false : 
     */
    private boolean testThread() {
    	int i = 0;
    	boolean b = true;
    	while(b) {
        	try {
    			Thread.sleep(1000);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
            	b = false;
            	return false;
    		}
        	Print.p("testThread 执行 testThread  " + i);
        	i++;
    	}
    	return true;
    }

    public  static void stopThread(Thread thread) {
		Print.p("testThread 结束线程");
		thread.interrupt();
    	
//		try {
//			Print.p("testThread 结束线程");
//			thread.interrupt();
//			Print.p("testThread join");
//			thread.join();
//			Print.p("testThread join over");
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			Print.p("testThread printStackTrace");
//
//		}
    }
}
